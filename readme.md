
# 基于SSH的通讯录管理系统
## 1、项目介绍

基于SSH的通讯录管理系统拥有两种角色：管理员和用户

管理员：用户管理、公告管理、系别管理、班级管理、通讯信息管理等

用户：拥有管理员中的查看功能、登录注册


## 2、项目技术

后端框架：SSH（Struts+Spring+Hibernate）

前端技术：jsp、css、JavaScript、JQuery

## 3、开发环境

- JAVA版本：JDK1.8
- IDE类型：IDEA、Eclipse都可运行
- tomcat版本：Tomcat 7-10版本均可
- 数据库类型：MySql（5.5-5.7、8.x版本都可） 
- 硬件环境：Windows 或者 Mac OS


## 4、功能介绍

### 4.1 登录和注册

![登录](https://img-blog.csdnimg.cn/img_convert/6740318def027c126542b570cc53dcce.jpeg)

![注册](https://img-blog.csdnimg.cn/img_convert/616e87ce14632e4fc87da6abe0078fb0.jpeg)

### 4.2 其他模块

![用户管理](https://img-blog.csdnimg.cn/img_convert/a9e260647e6c0690fbf6c250d7b1ea6f.jpeg)

![公告管理](https://img-blog.csdnimg.cn/img_convert/847074b423d2e5c1e8af3504918efce0.jpeg)

![系别管理](https://img-blog.csdnimg.cn/img_convert/62e28b794f4f0378fa5cdc9b0b99ff01.jpeg)

![班级管理](https://img-blog.csdnimg.cn/img_convert/dee469e81db4779ab7cd261bc1ad20b7.jpeg)

![通讯信息管理](https://img-blog.csdnimg.cn/img_convert/7f072d195cc2bcc5ddfcc39d9f10d8d3.jpeg)
### 获取方式&视频演示

下方扫一下，回复关键词：

![屏幕截图 2023-09-27 220641](https://img-blog.csdnimg.cn/img_convert/ba727c95e68b00c5bafc991bead73d00.png)
